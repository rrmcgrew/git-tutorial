# Git Tutorial

Using this code is easy! All you need to do is clone the repository to your local machine.
```
cd /path/to/your/workspace
git clone git@gitlab.com:rrmcgrew/git-tutorial.git
cd git-tutorial
```

EZPZ!
